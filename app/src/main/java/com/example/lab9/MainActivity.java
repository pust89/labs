package com.example.lab9;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences preferences;  //ссылка на объект-настройку
    private EditText editTextText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextText = findViewById(R.id.editTextText);
        preferences = getSharedPreferences(  //метод getSharedPreferences возвращает объект-настройку
                //с именем, которое содержится в первом параметре.
                getString(R.string.preferences_name),        //имя настройки здесь берется из строкового ресурса
                MODE_PRIVATE                                   //скрытый режим – только наше приложение может читать
        );
    }


    @Override
    protected void onResume() { //обработчик события, которое помещает активность на передний план
        super.onResume();              //вызов обработчика базового класса
        String prefKey1 = getString(R.string.pref_key_1); //получаем из строкового ресурса ключ
        String value1 = preferences.getString(prefKey1, "");  //извлекаем из настройки значение
        editTextText.setText(value1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        String value = editTextText.getText().toString();   //извлекаем сохраняемые значения из EditText-ов
        SharedPreferences.Editor editor = preferences.edit(); //с помощью метода edit объекта-активности
        //получаем объект-редактор editor
        editor.putString(getString(R.string.pref_key_1), value); //с помощью редактора помещаем в настройку
        //элемент.  Первый параметр putString – ключ           						                        //элемента, взятый из строкового ресурса,
        //второй параметр – значение элемента,
        //извлеченное из соответствующего EditText-а
        editor.commit();   //применяем изменения настройки
    }

}